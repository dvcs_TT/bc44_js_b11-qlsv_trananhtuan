var dssv = [];

// Get json once the web page is loaded
var dataJson = localStorage.getItem("DSSV_LOCAL");
// convert json to array
if (dataJson != null) {
  dssv = JSON.parse(dataJson);
  for (let i = 0; i < dataArr.length; i++) {
    var item = dataArr[i];
    var sv = new SinhVien (
      item.ma,
      item.ten,
      item.email,
      item.matKhau,
      item.toan,
      item.ly,
      item.hoa
    );
    dssv.push(sv);
  }

  renderDSSV(dssv);
}

function themSinhVien() {
  // Get info from the form
  var sv = layThongTinTuFrom();
  dssv.push(sv);
  // Convert dssv array to json
  var dataJson = JSON.stringify(dssv);
  // set get remove
  // Save json
  localStorage.setItem("DSSV_LOCAL", dataJson);
  // render to layout
  renderDSSV(dssv);
}

function xoaSV(id) {
  // Find index
  var viTri = -1;
  for (var i = 0; i < dssv.length; i++) {
    var sv = dssv[i];
    if (sv.ma == id) {
      viTri = i;
    }
  }
  dssv.splice(viTri, 1);
  renderDSSV(dssv);
}
function suaSV(id) {
  var viTri = dssv.findIndex(function(item) {
    console.log("hello");
    return item.ma == id;
  });
  if (viTri != -1) {
    // Stop user from editting student's ID
    document.getElementById("txtMaSV").disabled = true;
    showThongTinLenForm(dssv[viTri]);
  }
}
function capNhatSinhVien() {
  // Not stop user from editting student's ID
  document.getElementById("txtMaSV").disabled = false;

  // Get info from the form
  var sv = layThongTinTuForm();
  var viTri = dssv.findIndex(function(item) {
    return item.ma == sv.ma;
  });
  if (viTri !== -1) {
    dssv[viTri] = sv;
    renderDSSV(dssv);
  }
  function resetForm() {
    document.getElementById("formQLSV").reset();
  }
}